package edu.vt.adeloof;

import com.google.common.base.Throwables;
import javafx.application.Application;

import javax.swing.*;

/**
 * The main class which launches the JavaFX application
 */
public class AppLauncher {

    public static void main(String[] args) {
        try {
            Application.launch(HexEditorApp.class);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, Throwables.getStackTraceAsString(e), "Failed to launch app", JOptionPane.ERROR_MESSAGE);
        }
    }
}
