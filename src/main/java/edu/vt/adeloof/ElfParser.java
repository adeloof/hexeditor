package edu.vt.adeloof;

import edu.vt.adeloof.ui.StyleableVBox;
import edu.vt.adeloof.ui.TreeNode;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.*;
import javafx.util.Callback;

import java.util.Arrays;
import java.util.function.BiConsumer;

/**
 * Displays the fields of an ELF executable
 */
public class ElfParser extends ScrollPane {

    private static final byte[] MAGIC_BYTES = {0x7f, 0x45, 0x4c, 0x46};

    private byte[] bytes;
    private boolean littleEndian;
    private BiConsumer<Integer, Integer> action;

    public ElfParser(byte[] bytes, BiConsumer<Integer, Integer> action) throws ElfException {
        // check for ELF magic bytes
        if (Arrays.mismatch(bytes, MAGIC_BYTES) != MAGIC_BYTES.length) {
            throw new ElfException("Missing magic bytes");
        }

        this.bytes = bytes;
        this.action = action;
        littleEndian = getByte(5) == 1;
        boolean elf32 = getByte(4) == 1;
        int sysBytes = elf32 ? 4 : 8;

        StyleableVBox content = new StyleableVBox();
        setContent(content);
        setFitToWidth(true);
        setHbarPolicy(ScrollBarPolicy.NEVER);
        setVbarPolicy(ScrollBarPolicy.NEVER);

        // file header
        ElfFieldTable fileHeader = new ElfFieldTable();
        TreeNode<ElfFieldTable> fileHeaderDropdown = new TreeNode<>("File Header");
        fileHeaderDropdown.add(fileHeader);
        content.getChildren().add(fileHeaderDropdown);
        fileHeader.getItems().addAll(
                new ElfField("ei_mag", 0, 4, "0x7F454C46"),
                new ElfField("ei_class", 4, 1),
                new ElfField("ei_data", 5, 1),
                new ElfField("ei_version", 6, 1),
                new ElfField("ei_osabi", 7, 1),
                new ElfField("ei_abiversion", 8, 1),
                new ElfField("e_type", 0x10, 2),
                new ElfField("e_machine", 0x12, 2),
                new ElfField("e_version", 0x14, 4),
                new ElfField("e_entry", 0x18, sysBytes),
                new ElfField("e_phoff", elf32 ? 0x1c : 0x20, sysBytes),
                new ElfField("e_shoff", elf32 ? 0x20 : 0x28, sysBytes),
                new ElfField("e_flags", elf32 ? 0x24 : 0x30, 4),
                new ElfField("e_ehsize", elf32 ? 0x28 : 0x34, 2),
                new ElfField("e_phentsize", elf32 ? 0x2a : 0x36, 2),
                new ElfField("e_phnum", elf32 ? 0x2c : 0x38, 2),
                new ElfField("e_shentsize", elf32 ? 0x2e : 0x3a, 2),
                new ElfField("e_shnum", elf32 ? 0x30 : 0x3c, 2),
                new ElfField("e_shstrndx", elf32 ? 0x32 : 0x3e, 2)
        );

        // program header
        TreeNode<TreeNode<ElfFieldTable>> programHeaderDropdown = new TreeNode<>("Program Header");
        content.getChildren().add(programHeaderDropdown);
        int phoff = getInt(elf32 ? 0x1c : 0x20, sysBytes);
        int phentsize = getInt(elf32 ? 0x2a : 0x36, 2);
        int phnum = getInt(elf32 ? 0x2c : 0x38, 2);
        for (int i = 0; i < phnum; i++) {
            ElfFieldTable table = new ElfFieldTable();
            TreeNode<ElfFieldTable> tableDropdown = new TreeNode<>(ptypeToString(getInt(phoff, 4)));
            tableDropdown.add(table);
            programHeaderDropdown.add(tableDropdown);
            table.getItems().addAll(
                    new ElfField("p_type", phoff, 4),
                    new ElfField("p_offset", phoff + (elf32 ? 4 : 8), sysBytes),
                    new ElfField("p_vaddr", phoff + (elf32 ? 8 : 0x10), sysBytes),
                    new ElfField("p_paddr", phoff + (elf32 ? 0xc : 0x18), sysBytes),
                    new ElfField("p_filesz", phoff + (elf32 ? 0x10 : 0x20), sysBytes),
                    new ElfField("p_memsz", phoff + (elf32 ? 0x14 : 0x28), sysBytes),
                    new ElfField("p_align", phoff + (elf32 ? 0x1c : 0x30), sysBytes)
            );
            table.getItems().add(elf32 ? 6 : 1, new ElfField("p_flags", phoff + (elf32 ? 0x18 : 4), 4));
            phoff += phentsize;
        }

        // section header
        TreeNode<TreeNode<ElfFieldTable>> sectionHeaderDropdown = new TreeNode<>("Section Header");
        content.getChildren().add(sectionHeaderDropdown);
        int shoff = getInt(elf32 ? 0x20 : 0x28, sysBytes);
        int shentsize = getInt(elf32 ? 0x2e : 0x3a, 2);
        int shnum = getInt(elf32 ? 0x30 : 0x3c, 2);
        int shstrndx = getInt(elf32 ? 0x32 : 0x3e, 2);
        int shstrtab = getInt(shoff + shstrndx * shentsize + (elf32 ? 0x10 : 0x18), sysBytes);
        int dynsym, dynstr, symtab, strtab;
        dynsym = dynstr = symtab = strtab = -1;
        for (int i = 0; i < shnum; i++) {
            ElfFieldTable table = new ElfFieldTable();
            String name = getInt(shoff + 4, 4) != 0 ? getString(shstrtab + getInt(shoff, 4)) : "null";
            TreeNode<ElfFieldTable> tableDropdown = new TreeNode<>(name);
            tableDropdown.add(table);
            sectionHeaderDropdown.add(tableDropdown);
            table.getItems().addAll(
                    new ElfField("sh_name", shoff, 4),
                    new ElfField("sh_type", shoff + 4, 4),
                    new ElfField("sh_flags", shoff + 8, sysBytes),
                    new ElfField("sh_addr", shoff + (elf32 ? 0xc : 0x10), sysBytes),
                    new ElfField("sh_offset", shoff + (elf32 ? 0x10 : 0x18), sysBytes),
                    new ElfField("sh_size", shoff + (elf32 ? 0x14 : 0x20), sysBytes),
                    new ElfField("sh_link", shoff + (elf32 ? 0x18 : 0x28), 4),
                    new ElfField("sh_info", shoff + (elf32 ? 0x1c : 0x2c), 4),
                    new ElfField("sh_addralign", shoff + (elf32 ? 0x20 : 0x30), sysBytes),
                    new ElfField("sh_entsize", shoff + (elf32 ? 0x24 : 0x38), sysBytes)
            );
            switch (name) {
                case ".dynsym" -> dynsym = shoff;
                case ".symtab" -> symtab = shoff;
                case ".dynstr" -> dynstr = getInt(shoff + (elf32 ? 0x10 : 0x18), sysBytes);
                case ".strtab" -> strtab = getInt(shoff + (elf32 ? 0x10 : 0x18), sysBytes);
            }
            shoff += shentsize;
        }

        // symbols
        TreeNode<TreeNode<ElfSymbolTable>> symbolsDropdown = new TreeNode<>("Symbols");
        content.getChildren().add(symbolsDropdown);
        if (dynsym != -1 && dynstr != -1) {
            ElfSymbolTable table = new ElfSymbolTable();
            TreeNode<ElfSymbolTable> dynsymDropdown = new TreeNode<>(".dynsym");
            symbolsDropdown.add(dynsymDropdown);
            dynsymDropdown.add(table);
            int sh_offset = getInt(dynsym + (elf32 ? 0x10 : 0x18), sysBytes);
            int sh_size = getInt(dynsym + (elf32 ? 0x14 : 0x20), sysBytes);
            int sh_entsize = getInt(dynsym + (elf32 ? 0x24 : 0x38), sysBytes);
            for (int i = 0; i < sh_size / sh_entsize; i++) {
                int st_name = getInt(sh_offset, 4);
                if (st_name != 0) {
                    int st_info = getInt(sh_offset + (elf32 ? 0xc : 4), 1);
                    table.getItems().add(new ElfSymbol(getString(dynstr + st_name), stinfoToBindingString(st_info), stinfoToTypeString(st_info)));
                }
                sh_offset += sh_entsize;
            }
        }
        if (symtab != -1 && strtab != -1) {
            ElfSymbolTable table = new ElfSymbolTable();
            TreeNode<ElfSymbolTable> symtabDropdown = new TreeNode<>(".symtab");
            symbolsDropdown.add(symtabDropdown);
            symtabDropdown.add(table);
            int sh_offset = getInt(symtab + (elf32 ? 0x10 : 0x18), sysBytes);
            int sh_size = getInt(symtab + (elf32 ? 0x14 : 0x20), sysBytes);
            int sh_entsize = getInt(symtab + (elf32 ? 0x24 : 0x38), sysBytes);
            for (int i = 0; i < sh_size / sh_entsize; i++) {
                int st_name = getInt(sh_offset, 4);
                if (st_name != 0) {
                    int st_info = getInt(sh_offset + (elf32 ? 0xc : 4), 1);
                    table.getItems().add(new ElfSymbol(getString(strtab + st_name), stinfoToBindingString(st_info), stinfoToTypeString(st_info)));
                }
                sh_offset += sh_entsize;
            }
        }
    }

    private byte getByte(int offset) throws ElfException {
        if (offset >= 0 && offset < bytes.length) {
            return bytes[offset];
        }
        throw new ElfException("Invalid format");
    }

    private String getBytes(int offset, int size) throws ElfException {
        StringBuilder builder = new StringBuilder();
        if (littleEndian) {
            offset += size - 1;
        }
        for (int i = 0; i < size; i++) {
            builder.append(String.format("%02X", getByte(offset)));
            offset += littleEndian ? -1 : 1;
        }
        return builder.toString();
    }

    private int getInt(int offset, int size) throws ElfException {
        return Integer.parseInt(getBytes(offset, size), 16);
    }

    private String getString(int offset) {
        StringBuilder builder = new StringBuilder();
        while (bytes[offset] != 0) {
            builder.append((char) bytes[offset++]);
        }
        return builder.toString();
    }

    private String ptypeToString(int type) {
        return switch (type) {
            case 0 -> "PT_NULL";
            case 1 -> "PT_LOAD";
            case 2 -> "PT_DYNAMIC";
            case 3 -> "PT_INTERP";
            case 4 -> "PT_NOTE";
            case 5 -> "PT_SHLIB";
            case 6 -> "PT_PHDR";
            case 7 -> "PT_TLS";
            case 0x60000000 -> "PT_LOOS";
            case 0x6FFFFFFF -> "PT_HIOS";
            case 0x70000000 -> "PT_LOPROC";
            case 0x7FFFFFFF -> "PT_HIPROC";
            default -> "Unknown";
        };
    }

    private String stinfoToBindingString(int i) {
        return switch (i >> 4) {
            case 0 -> "LOCAL";
            case 1 -> "GLOBAL";
            case 2 -> "WEAK";
            case 10 -> "LOOS";
            case 12 -> "HIOS";
            case 13 -> "LOPROC";
            case 15 -> "HIPROC";
            default -> "Unknown";
        };
    }

    private String stinfoToTypeString(int i) {
        return switch (i & 0xf) {
            case 0 -> "NOTYPE";
            case 1 -> "OBJECT";
            case 2 -> "FUNC";
            case 3 -> "SECTION";
            case 4 -> "FILE";
            case 5 -> "COMMON";
            case 6 -> "TLS";
            case 10 -> "LOOS";
            case 12 -> "HIOS";
            case 13 -> "LOPROC";
            case 15 -> "HIPROC";
            default -> "Unknown";
        };
    }

    private class ElfFieldTable extends TableView<ElfField> {

        @SuppressWarnings("unchecked")
        private ElfFieldTable() {
            setEditable(false);
            setSortPolicy(o -> false);
            TableColumn<ElfField, String> nameColumn = new TableColumn<>("Field");
            TableColumn<ElfField, String> offsetColumn = new TableColumn<>("Offset");
            TableColumn<ElfField, String> valueColumn = new TableColumn<>("Value");
            nameColumn.setCellValueFactory(o -> o.getValue().nameString);
            offsetColumn.setCellValueFactory(o -> o.getValue().offsetString);
            valueColumn.setCellValueFactory(o -> o.getValue().valueString);
            getColumns().addAll(nameColumn, offsetColumn, valueColumn);

            offsetColumn.setCellFactory(new Callback<>() {
                @Override
                public TableCell<ElfField, String> call(TableColumn<ElfField, String> param) {
                    TableCell<ElfField, String> cell = new TableCell<>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(empty ? null : item);
                        }
                    };
                    cell.setOnMouseClicked(e -> {
                        if (e.getClickCount() == 2) {
                            ElfField field = cell.getTableRow().getItem();
                            action.accept(field.offset, field.size);
                        }
                    });
                    return cell;
                }
            });

            valueColumn.setCellFactory(new Callback<>() {
                @Override
                public TableCell<ElfField, String> call(TableColumn<ElfField, String> param) {
                    TableCell<ElfField, String> cell = new TableCell<>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(empty ? null : item);
                        }
                    };
                    cell.setOnMouseClicked(e -> {
                        if (e.getClickCount() == 2) {
                            ElfField field = cell.getTableRow().getItem();
                            try {
                                action.accept(getInt(field.offset, field.size), 1);
                            } catch (ElfException ignored) {}
                        }
                    });
                    return cell;
                }
            });
        }
    }

    public class ElfField {

        private StringProperty nameString;
        private StringProperty offsetString;
        private StringProperty valueString;
        private int offset;
        private int size;

        private ElfField(String name, int offset, int size, String value) {
            this.offset = offset;
            this.size = size;
            nameString = new SimpleStringProperty(name);
            offsetString = new SimpleStringProperty(String.format("0x%08X", offset));
            valueString = new SimpleStringProperty(value);
        }

        private ElfField(String name, int offset, int size) throws ElfException {
            this(name, offset, size, "0x" + getBytes(offset, size));
        }
    }

    private static class ElfSymbolTable extends TableView<ElfSymbol> {

        @SuppressWarnings("unchecked")
        private ElfSymbolTable() {
            setEditable(false);
            setSortPolicy(o -> false);
            TableColumn<ElfSymbol, String> nameColumn = new TableColumn<>("Name");
            TableColumn<ElfSymbol, String> bindingColumn = new TableColumn<>("Binding");
            TableColumn<ElfSymbol, String> typeColumn = new TableColumn<>("Type");
            nameColumn.setCellValueFactory(o -> o.getValue().nameString);
            bindingColumn.setCellValueFactory(o -> o.getValue().bindingString);
            typeColumn.setCellValueFactory(o -> o.getValue().typeString);
            getColumns().addAll(nameColumn, bindingColumn, typeColumn);
        }
    }

    private static class ElfSymbol {

        private StringProperty nameString;
        private StringProperty bindingString;
        private StringProperty typeString;

        private ElfSymbol(String name, String binding, String type) {
            nameString = new SimpleStringProperty(name);
            bindingString = new SimpleStringProperty(binding);
            typeString = new SimpleStringProperty(type);
        }
    }

    public static class ElfException extends Exception {

        public ElfException(String message) {
            super(message);
        }
    }
}
