package edu.vt.adeloof.ui;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;

public class TreeNode<T extends Node> extends StyleableVBox {

    private Label label = new Label();
    private StyleableVBox content = new StyleableVBox();
    private String collapsedText;
    private String expandedText;
    private boolean expanded = true;

    public TreeNode(String labelText) {
        collapsedText = "▸ " + labelText;
        expandedText = "▾ " + labelText;

        label.setOnMouseClicked(e -> {
            if (e.getButton() == MouseButton.PRIMARY) {
                toggle();
                e.consume();
            }
        });

        getStyleClass().add("tree-node");
        getChildren().addAll(label, content);
        toggle();
    }

    public void toggle() {
        expanded = !expanded;
        label.setText(expanded ? expandedText : collapsedText);
        content.setVisible(expanded);
        content.setManaged(expanded);
    }

    public void add(T node) {
        content.getChildren().add(node);
    }
}
