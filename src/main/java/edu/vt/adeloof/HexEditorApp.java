package edu.vt.adeloof;

import com.google.common.base.Throwables;
import edu.vt.adeloof.ui.ActionMenuItem;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.input.DataFormat;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The JavaFX application class
 */
public class HexEditorApp extends Application {

    private static HexEditorApp instance;

    private ExecutorService executorService = Executors.newCachedThreadPool();
    private BorderPane rootPane = new BorderPane();
    private TabPane tabPane = new TabPane();
    private Scene scene = new Scene(rootPane, 750, 500);
    private Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        instance = this;
        this.stage = stage;

        // allow dragging and dropping files into the editor
        scene.setOnDragOver(e -> {
            if (e.getDragboard().hasContent(DataFormat.FILES)) {
                e.acceptTransferModes(TransferMode.COPY);
            }
        });
        scene.setOnDragDropped(e -> {
            for (File file : e.getDragboard().getFiles()) {
                openFile(file.toPath());
            }
        });
        rootPane.centerProperty().bind(Bindings
                .when(tabPane.getSelectionModel().selectedItemProperty().isNull())
                .then((Node) new Label("<drag and drop a file here>"))
                .otherwise(tabPane));

        // prompt on exit
        stage.setOnCloseRequest(e -> {
            exitPrompt();
            e.consume();
        });

        // prepare the GUI and display it
        tabPane.setTabDragPolicy(TabPane.TabDragPolicy.REORDER);
        scene.getStylesheets().add("style.css");
        rootPane.setTop(createMenuBar());
        stage.setTitle("Hex Editor");
        stage.setScene(scene);
        try (InputStream is = HexEditorApp.class.getResourceAsStream("/icon.png")) {
            stage.getIcons().add(new Image(is));
            stage.show();
        }
    }

    @Override
    public void stop() throws Exception {
        executorService.shutdown();
    }

    private void exitPrompt() {
        if (currentTab() == null) {
            Platform.exit();
            return;
        }
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to exit?\n(any unsaved changes will be lost)", ButtonType.YES, ButtonType.CANCEL);
        alert.setHeaderText(null);
        alert.setGraphic(null);
        alert.showAndWait().ifPresent(buttonType -> {
            if (buttonType == ButtonType.YES) {
                Platform.exit();
            }
        });
    }

    private MenuBar createMenuBar() {
        // file menu
        Menu fileMenu = new Menu("File");
        ActionMenuItem openItem = new ActionMenuItem("Open", e-> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
            List<File> files = fileChooser.showOpenMultipleDialog(stage);
            if (files != null) {
                for (File file : files) {
                    openFile(file.toPath());
                }
            }
        });
        ActionMenuItem saveItem = new ActionMenuItem("Save", e -> currentTab().save());
        ActionMenuItem saveAsItem = new ActionMenuItem("Save As", e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
            File file = fileChooser.showSaveDialog(stage);
            if (file != null) {
                currentTab().save(file.toPath());
            }
        });
        ActionMenuItem exitItem = new ActionMenuItem("Exit", e -> exitPrompt());
        fileMenu.getItems().addAll(openItem, saveItem, saveAsItem, exitItem);

        // view menu
        Menu viewMenu = new Menu("View");
        ActionMenuItem editorItem = new ActionMenuItem("Editor", e -> currentTab().openEditorView());
        ActionMenuItem elfItem = new ActionMenuItem("ELF", e -> currentTab().openElfView());
        viewMenu.getItems().addAll(editorItem, elfItem);

        // help menu
        Menu helpMenu = new Menu("Help");
        ActionMenuItem gitlabMenuItem = new ActionMenuItem("Gitlab", e-> {
            try {
                Desktop.getDesktop().browse(URI.create("https://git.cs.vt.edu/adeloof/hexeditor"));
            } catch (IOException ex) {
                displayException("Failed to open URL", ex);
            }
        });
        helpMenu.getItems().addAll(gitlabMenuItem);

        for (MenuItem menuItem : new MenuItem[]{saveItem, saveAsItem, editorItem, elfItem}) {
            menuItem.disableProperty().bind(tabPane.getSelectionModel().selectedItemProperty().isNull());
        }
        return new MenuBar(fileMenu, viewMenu, helpMenu);
    }

    public void openFile(Path file) {
        FileDisplayTab tab = new FileDisplayTab(file);
        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);
    }

    public void displayNotification(String message, boolean error) {
        Notifications n = Notifications.create().owner(stage).text(message).hideAfter(Duration.seconds(5));
        if (error) {
            n.showError();
        } else {
            n.showInformation();
        }
    }

    public void displayException(String message, Throwable e) {
        Toolkit.getDefaultToolkit().beep();
        Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.CLOSE);
        TextArea textArea = new TextArea(Throwables.getStackTraceAsString((e)));
        textArea.setEditable(false);
        alert.getDialogPane().setContent(new VBox(5, new Label(message), textArea));
        alert.setHeaderText(null);
        alert.setGraphic(null);
        alert.showAndWait();
    }

    public FileDisplayTab currentTab() {
        return tabPane.getSelectionModel().isEmpty() ? null : (FileDisplayTab) tabPane.getSelectionModel().getSelectedItem();
    }

    public static HexEditorApp getInstance() {
        return instance;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    public BorderPane getRootPane() {
        return rootPane;
    }

    public TabPane getTabPane() {
        return tabPane;
    }

    public Scene getScene() {
        return scene;
    }

    public Stage getStage() {
        return stage;
    }
}
