package edu.vt.adeloof;

import com.google.common.base.CharMatcher;
import com.google.common.primitives.UnsignedBytes;
import edu.vt.adeloof.ui.StyleableHBox;
import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.Caret;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.Selection;
import org.fxmisc.richtext.SelectionImpl;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.HashSet;
import java.util.HexFormat;
import java.util.Set;

/**
 * Displays the bytes of a file
 */
public class FileDisplayTab extends Tab {

    private static Image loadingIcon;

    private Path file;
    private byte[] bytes;
    private BorderPane rootPane;
    private ByteArea byteArea;
    private StyleableHBox searchBox = new StyleableHBox(10);
    private TextField searchField = new TextField();
    private RadioButton searchStrCheckBox = new RadioButton("String");
    private RadioButton searchBytesCheckBox = new RadioButton("Bytes");
    private RadioButton searchOffsetCheckBox = new RadioButton("Offset");
    private int searchIndex;

    static {
        try (InputStream is = FileDisplayTab.class.getResourceAsStream("/loading.gif")) {
            loadingIcon = new Image(is);
        } catch (IOException e) {
            HexEditorApp.getInstance().displayException("Failed to load resource", e);
        }
    }

    public FileDisplayTab(Path file) {
        this.file = file;
        setText(file.getFileName().toString());
        setContent(rootPane = new BorderPane());
        rootPane.setCenter(new ImageView(loadingIcon));

        Button findButton = new Button("Find");
        findButton.setOnAction(e -> find());
        findButton.disableProperty().bind(searchField.textProperty().isEmpty());
        ToggleGroup searchTypeGroup = new ToggleGroup();
        searchTypeGroup.getToggles().addAll(searchStrCheckBox, searchBytesCheckBox, searchOffsetCheckBox);
        searchTypeGroup.selectToggle(searchStrCheckBox);
        searchBox.getChildren().addAll(new Label("Search:"), searchField, findButton, searchStrCheckBox, searchBytesCheckBox, searchOffsetCheckBox);
        searchField.textProperty().addListener((observable, oldValue, newValue) -> searchIndex = 0);

        setOnCloseRequest(e -> {
            ButtonType saveButton = new ButtonType("Save and Close");
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Do you want to save the changes?", saveButton, new ButtonType("Close w/o Saving"));
            alert.setHeaderText(null);
            alert.setGraphic(null);
            alert.showAndWait().ifPresent(buttonType -> {
                if (buttonType == saveButton) {
                    save();
                }
            });
        });

        HexEditorApp.getInstance().getExecutorService().submit(this::loadFromFile);
    }

    private void loadFromFile() {
        // read file bytes
        try {
            bytes = Files.readAllBytes(file);
        } catch (IOException e) {
            Platform.runLater(() -> HexEditorApp.getInstance().displayException("Failed to read file", e));
            return;
        }
        // prepare display content
        StringBuilder contentBuilder = new StringBuilder();
        for (int offset = 0; offset < bytes.length; offset += 16) {
            StringBuilder hexBuilder = new StringBuilder();
            StringBuilder charBuilder = new StringBuilder();
            int i;
            for (i = 0; i < 16 && offset + i < bytes.length; i++) {
                byte b = bytes[offset + i];
                if (i != 0) {
                    hexBuilder.append(' ');
                }
                hexBuilder.append(byteToHex(b));
                charBuilder.append(byteToChar(b));
            }
            contentBuilder.append(String.format("%08X:   ", offset)).append(hexBuilder).append("   ").append("   ".repeat(16 - i)).append(charBuilder).append(" ".repeat(16 - i)).append('\n');
        }
        // add to GUI
        String content = contentBuilder.toString();
        Platform.runLater(() -> {
            byteArea = new ByteArea(content);
            byteArea.moveTo(DisplaySection.HEX.offset);
            byteArea.requestFollowCaret();
            rootPane.setCenter(new VirtualizedScrollPane<>(byteArea));
            rootPane.setBottom(searchBox);
        });
    }

    public void openEditorView() {
        rootPane.setCenter(new VirtualizedScrollPane<>(byteArea));
        rootPane.setBottom(searchBox);
    }

    public void openElfView() {
        try {
            rootPane.setCenter(new ElfParser(bytes, (offset, size) -> {
                if (offset + size <= bytes.length) {
                    byteArea.selectBytes(offset, offset + size);
                    openEditorView();
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            }));
            rootPane.setBottom(null);
        } catch (ElfParser.ElfException e) {
            HexEditorApp.getInstance().displayNotification("Invalid ELF executable", true);
        }
    }

    public void save(Path file) {
        HexEditorApp.getInstance().getExecutorService().submit(() -> {
            try {
                Files.write(file, bytes, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            } catch (IOException e) {
                Platform.runLater(() -> HexEditorApp.getInstance().displayException("Failed to save", e));
            }
        });
    }

    public void save() {
        save(file);
    }

    public void find() {
        String searchStr = searchField.getText();
        if (searchOffsetCheckBox.isSelected()) {
            try {
                int byteIndex = Integer.parseInt(searchStr, 16);
                if (byteIndex < bytes.length) {
                    byteArea.selectBytes(byteIndex, byteIndex + 1);
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            } catch (NumberFormatException e) {
                HexEditorApp.getInstance().displayNotification("Invalid offset", true);
            }
        } else {
            byte[] target;
            if (searchBytesCheckBox.isSelected()) {
                try {
                    target = HexFormat.of().parseHex(searchStr.replace(" ", ""));
                } catch (IllegalArgumentException e) {
                    HexEditorApp.getInstance().displayNotification("Invalid bytes", true);
                    return;
                }
            } else {
                target = searchStr.getBytes(StandardCharsets.UTF_8);
            }
            int i = find(target);
            if (i != -1) {
                byteArea.selectBytes(i, i + target.length);
            }
        }
    }

    private int find(byte[] target) {
        for (int i = searchIndex; i < bytes.length; i++) {
            if (target[0] == bytes[i]) {
                boolean match = true;
                for (int j = 1; j < target.length && i + j < bytes.length; j++) {
                    if (target[j] != bytes[i + j]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    searchIndex = i + target.length;
                    return i;
                }
            }
        }
        if (searchIndex != 0) {
            searchIndex = 0;
            return find(target);
        }
        Toolkit.getDefaultToolkit().beep();
        return -1;
    }

    // util methods
    private String byteToHex(byte b) {
        return String.format("%02X", b);
    }
    private String byteToChar(byte b) {
        return b >= 33 && b <= 126 ? String.valueOf((char) b) : ".";
    }
    private int roundDown(int n, int x) {
        return (n / x) * x;
    }
    private int roundUp(int n, int x) {
        return roundDown(n, x) + x;
    }

    /**
     * Displays the offset, hex, and character, section
     */
    private class ByteArea extends CodeArea {

        private static int LINE_LEN = 79;
        private Set<Selection<Collection<String>, String, Collection<String>>> selections = new HashSet<>();

        @SuppressWarnings("UnstableApiUsage")
        private ByteArea(String content) {
            super(content);
            setShowCaret(Caret.CaretVisibility.ON);
            setOnMouseClicked(e -> clearSelections());

            addEventFilter(KeyEvent.KEY_TYPED, e -> {
                int caret = getCaretPosition();
                if (caret % LINE_LEN == DisplaySection.HEX.offset + DisplaySection.HEX.len) {
                    caret = roundDown(caret, LINE_LEN) + LINE_LEN + DisplaySection.HEX.offset;
                } else if (caret % LINE_LEN == DisplaySection.CHAR.offset + DisplaySection.CHAR.len) {
                    caret = roundDown(caret, LINE_LEN) + LINE_LEN + DisplaySection.CHAR.offset;
                }
                DisplaySection section = positionToDisplaySection(caret);
                if (section == DisplaySection.HEX) {
                    // typed in hex section
                    if (e.getCharacter().length() == 1 && HexFormat.isHexDigit(e.getCharacter().charAt(0))) {
                        int byteIndex = positionToByteIndex(caret);
                        if (byteIndex != -1) {
                            if (getText(caret, caret + 1).equals(" ")) {
                                caret++;
                            }
                            replaceText(caret, caret + 1, e.getCharacter().toUpperCase());
                            int bytePos = byteIndexToPosition(byteIndex, DisplaySection.HEX);
                            byte newByte = UnsignedBytes.parseUnsignedByte(getText(bytePos, bytePos + 2), 16);
                            setByte(byteIndex, newByte);
                            moveTo(caret + 1);
                        }
                    }
                } else if (section == DisplaySection.CHAR) {
                    // typed in char section
                    int byteIndex = positionToByteIndex(caret);
                    if (byteIndex != -1 && e.getCharacter().length() == 1) {
                        setByte(byteIndex, (byte) e.getCharacter().charAt(0));
                        moveTo(caret + 1);
                    }
                }
                e.consume();
            });
        }

        private void setByte(int i, byte b) {
            bytes[i] = b;
            int pos1 = byteIndexToPosition(i, DisplaySection.HEX);
            int pos2 = byteIndexToPosition(i, DisplaySection.CHAR);
            replaceText(pos1, pos1 + 2, byteToHex(b));
            replaceText(pos2, pos2 + 1, byteToChar(b));
            setStyleClass(pos1, pos1 + 2, "modified-bytes");
            setStyleClass(pos2, pos2 + 1, "modified-bytes");
        }

        private void selectBytes(int start, int end) {
            clearSelections();
            while (start < end) {
                Selection<Collection<String>, String, Collection<String>> hexSelection = new SelectionImpl<>("byte" + start, byteArea);
                Selection<Collection<String>, String, Collection<String>> charSelection = new SelectionImpl<>("char" + start, byteArea);
                addSelection(hexSelection);
                addSelection(charSelection);
                selections.add(hexSelection);
                selections.add(charSelection);
                int selectionEnd = Math.min(roundUp(start, 16), end);
                hexSelection.selectRange(byteIndexToPosition(start, DisplaySection.HEX), byteIndexToPosition(selectionEnd - 1, DisplaySection.HEX) + 2);
                charSelection.selectRange(byteIndexToPosition(start, DisplaySection.CHAR), byteIndexToPosition(selectionEnd - 1, DisplaySection.CHAR) + 1);
                moveTo(hexSelection.getEndPosition());
                start = selectionEnd;
            }
            requestFollowCaret();
        }

        private void clearSelections() {
            for (Selection<Collection<String>, String, Collection<String>> selection : selections) {
                selection.deselect();
                selection.dispose();
                removeSelection(selection);
            }
            selections.clear();
        }

        private DisplaySection positionToDisplaySection(int pos) {
            int offsetInLine = pos % ByteArea.LINE_LEN;
            if (offsetInLine >= DisplaySection.HEX.offset) {
                if (offsetInLine < DisplaySection.HEX.offset + DisplaySection.HEX.len) {
                    return DisplaySection.HEX;
                }
                if (offsetInLine >= DisplaySection.CHAR.offset && offsetInLine < DisplaySection.CHAR.offset + DisplaySection.CHAR.len) {
                    return DisplaySection.CHAR;
                }
            }
            return DisplaySection.NONE;
        }

        private int positionToByteIndex(int pos) {
            DisplaySection section = positionToDisplaySection(pos);
            int i = -1;
            if (section == DisplaySection.HEX) {
                i = (pos / ByteArea.LINE_LEN) * 16 + CharMatcher.is(' ').countIn(byteArea.getText(roundDown(pos, ByteArea.LINE_LEN) + section.offset, pos + 1));
            } else if (section == DisplaySection.CHAR) {
                i = (pos / ByteArea.LINE_LEN) * 16 + (pos - (roundDown(pos, ByteArea.LINE_LEN) + section.offset));
            }
            return i >= 0 && i < bytes.length ? i : -1;
        }

        private int byteIndexToPosition(int i, DisplaySection section) {
            return (i / 16) * ByteArea.LINE_LEN + section.offset + section.spacing * (i - roundDown(i, 16));
        }

        // override these methods to do nothing
        @Override
        public void deleteNextChar() {}
        @Override
        public void deletePreviousChar() {}
        @Override
        public void replaceSelection(String replacement) {}
        @Override
        public void moveSelectedText(int position) {}
        @Override
        public void paste() {}
        @Override
        public void undo() {}
        @Override
        public void redo() {}
    }

    private enum DisplaySection {

        HEX(12, 47, 3),
        CHAR(62, 16, 1),
        NONE(0, 0, 0);

        private final int offset;
        private final int len;
        private final int spacing;

        DisplaySection(int offset, int len, int spacing) {
            this.offset = offset;
            this.len = len;
            this.spacing = spacing;
        }
    }
}
